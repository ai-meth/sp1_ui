#!/bin/sh
source /home/ubuntu/dev_ws/install/setup.bash
cd /home/ubuntu/guiv1.0b/client

ros2 launch step_launch step.launch.py | tee 1.log | sed -e 's/^/[ROS] /' & npm run serve | tee 2.log | sed -e 's/^/[NODE] /' & ros2 launch rosbridge_server rosbridge_websocket_launch.xml | tee 2.log | sed -e 's/^/[ROS-SERVER] /'