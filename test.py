from evdev import *
import time

dev = None

# Find first EV_FF capable event device (that we have permissions to use).
for name in util.list_devices():
    dev = InputDevice(name)
    if ecodes.EV_FF in dev.capabilities():
        break

if dev is None:
    
    print("Sorry, no FF capable device found")
    
else:
    print("found " + dev.name + " at " + dev.path)

    dev.capabilities(verbose=True)